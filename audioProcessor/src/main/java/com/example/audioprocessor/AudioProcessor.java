package com.example.audioprocessor;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class AudioProcessor {

    static {
        System.loadLibrary("audio-processor");
    }

    private final int mBufferSize;
    private final byte[] mOutBuffer;
    private final float[] mFloatInput;
    private final float[] mFloatOutput;
    HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "AudioProcessor");

    public AudioProcessor(int bufferSize) {
        mBufferSize = bufferSize;
        mOutBuffer = new byte[mBufferSize];
        // in native code, two bytes is converted into one float
        mFloatInput = new float[mBufferSize / 2];
        mFloatOutput = new float[mBufferSize / 2];
    }

    private static native void process(float ratio, byte[] in, byte[] out,
                                       int size, int sampleRate, float[] floatInput, float[] floatOutput);

    /**
     * @param ratio 0~2
     */
    public synchronized byte[] process(float ratio, byte[] input, int sampleRate) {
        process(ratio, input, mOutBuffer, mBufferSize, sampleRate, mFloatInput, mFloatOutput);
        HiLog.warn(label, "AudioProcessor outBuffer:" + mOutBuffer);
        return mOutBuffer;
    }
}
