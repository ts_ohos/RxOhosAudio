package com.example.rxohosaudio.ability;

import com.example.rxohosaudio.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

public class MainAbility extends Ability {

    @Override
    public void onStart(final Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        Button button = (Button) findComponentById(ResourceTable.Id_mBtnFileMode);
        Button button1 = (Button) findComponentById(ResourceTable.Id_mBtnStreamMode);

        if (button != null) {
            button.setClickedListener(component -> {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.rxohosaudio")
                        .withAbilityName("com.example.rxohosaudio.ability.FileAbility")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            });
        }

        if (button1 != null) {
            button1.setClickedListener(component -> {
                Intent secondintent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.rxohosaudio")
                        .withAbilityName("com.example.rxohosaudio.ability.StreamAbility")
                        .build();
                secondintent.setOperation(operation);
                startAbility(secondintent);
            });
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(final Intent intent) {
        super.onForeground(intent);
    }
}
