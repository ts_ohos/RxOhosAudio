# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
- empty
### Fixed
- upgrade API version to 5.

## [2.0.0] - 2021-04-19
### Added
- The initial release.

[2.0.0]: https://gitee.com/ts_ohos/RxOhosAudio/tree/v2.0.0