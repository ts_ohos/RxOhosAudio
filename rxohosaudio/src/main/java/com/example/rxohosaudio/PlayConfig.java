package com.example.rxohosaudio;

import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.media.audio.AudioManager;
import ohos.utils.net.Uri;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SuppressWarnings({"unused", "WeakerAccess"})
public final class PlayConfig {
    static final int TYPE_FILE = 1;
    static final int TYPE_RES = 2;
    static final int TYPE_URL = 3;
    static final int TYPE_URI = 4;

    @PlayConfig.Type
    final int mType;

    final Context mContext;

    final RawFileEntry mAudioResource;

    final Uri mUri;

    final File mAudioFile;

    final String mUrl;

    final int mStreamType;

    final boolean mLooping;

    //    @FloatRange(from = 0.0F, to = 1.0F)
    final float mLeftVolume;

    //    @FloatRange(from = 0.0F, to = 1.0F)
    final float mRightVolume;

    private PlayConfig(final Builder builder) {
        mType = builder.mType;
        mContext = builder.mContext;
        mAudioResource = builder.mAudioResource;
        mAudioFile = builder.mAudioFile;
        mStreamType = builder.mStreamType;
        mLooping = builder.mLooping;
        mLeftVolume = builder.mLeftVolume;
        mRightVolume = builder.mRightVolume;
        mUri = builder.mUri;
        mUrl = builder.mUrl;
    }

    public static Builder file(final File file) {
        Builder builder = new Builder();
        builder.mAudioFile = file;
        builder.mType = TYPE_FILE;
        return builder;
    }

    public static Builder url(final String url) {
        Builder builder = new Builder();
        builder.mUrl = url;
        builder.mType = TYPE_URL;
        return builder;
    }

    //todo:
    //    public static Builder res(Context context, @RawRes int audioResource) {
    public static Builder res(final Context context, final RawFileEntry audioResource) {
        Builder builder = new Builder();
        builder.mContext = context;
        builder.mAudioResource = audioResource;
        builder.mType = TYPE_RES;
        return builder;
    }

    public static Builder uri(final Context context, final Uri uri) {
        Builder builder = new Builder();
        builder.mContext = context;
        builder.mUri = uri;
        builder.mType = TYPE_URI;
        return builder;
    }

    boolean isArgumentValid() {
        switch (mType) {
            case TYPE_FILE:
                return mAudioFile != null && mAudioFile.exists();
            case TYPE_RES:
                return mAudioResource != null && mContext != null;
            case TYPE_URL:
                return !(mUrl == null || mUrl.length() == 0);
            case TYPE_URI:
                return mUri != null;
            default:
                return false;
        }
    }

    boolean isLocalSource() {
        switch (mType) {
            case PlayConfig.TYPE_FILE:
            case PlayConfig.TYPE_RES:
                return true;
            case PlayConfig.TYPE_URL:
            case PlayConfig.TYPE_URI:
            default:
                return false;
        }
    }

    boolean needPrepare() {
        switch (mType) {
            case PlayConfig.TYPE_FILE:
            case PlayConfig.TYPE_URL:
            case PlayConfig.TYPE_URI:
                return true;
            case PlayConfig.TYPE_RES:
            default:
                return false;
        }
    }

    @IntDef(value = {TYPE_FILE, TYPE_RES, TYPE_URL, TYPE_URI})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    public static class Builder {

        @PlayConfig.Type
        private int mType;

        private Context mContext;

        //  todo:      @RawRes
        private RawFileEntry mAudioResource;

        private Uri mUri;

        private File mAudioFile;

        private String mUrl;

        private int mStreamType = 3;

        private boolean mLooping;
        //   todo:     @FloatRange(from = 0.0F, to = 1.0F)
        private float mLeftVolume = 1.0F;

        //    todo:    @FloatRange(from = 0.0F, to = 1.0F)
        private float mRightVolume = 1.0F;

        /**
         * {AudioManager.STREAM_VOICE_CALL} etc.
         *
         * @param streamType
         */
        public Builder streamType(final AudioManager.AudioVolumeType streamType) {
            mStreamType = streamType.getValue();
            return this;
        }

        public Builder looping(boolean looping) {
            mLooping = looping;
            return this;
        }

        //  todo:      public Builder leftVolume(@FloatRange(from = 0.0F, to = 1.0F) float leftVolume) {
        public Builder leftVolume(final float leftVolume) {
            mLeftVolume = leftVolume;
            return this;
        }

        //    todo：    public Builder rightVolume(@FloatRange(from = 0.0F, to = 1.0F) float rightVolume) {
        public Builder rightVolume(final float rightVolume) {
            mRightVolume = rightVolume;
            return this;
        }

        public PlayConfig build() {
            return new PlayConfig(this);
        }
    }
}
