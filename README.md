# RxAndroidAudio

Adaptation of RxAndroidAudio(https://github.com/Piasy/RxAndroidAudio) to ohos.An Audio encapsulation library, with part Rx support.

本项目是基于开源项目Piasy/RxAndroidAudio进行harmonyos移植和开发的，可以通过地址https://github.com/Piasy/RxAndroidAudio 追踪到原安卓项目版本。

移植版本：v1.7.0

## 项目介绍
## (Project Introduce)
### 项目名称（Project Name:）：RxAndroidAudio
### 所属系列：鸿蒙的第三方组件适配移植
### 功能：(Functions:)
1. 实现音频文件录制和播放
(1.record and play audio file)
2. 实现音频流文件录制和播放
(2.record and play audio stream file)
3. 变声播放录音文件
(3.voice changing play)

### 项目移植状态：(project complete status:)
1. 主要功能已经移植；
(1.mainly functions completed)
2. 测试Demo可以正常使用；
(2.Demo for test can be use)

### 编程语言：java

## 集成(How to use:)
方式一：
1. 下载或自行编译生成RxAndroidAudio的.har文件，文件路径为：entry/libs/rxohosaudio.har。
(1.Download or compile har file yourself,the sample har file location:entry/libs/rxohosaudio.har.)
2. 自行编译时，需要注意要自行添加签名。
(2.You should pay attention to add the Signature file to project structure when you compile the project your self.)
3. 导入你的OHOS项目模块的**./libs**中。
(3.import your OHOS projct module to **./libs** of your project.)
4. 在模块下的**build.gradle**中确认依赖**./libs**下的.har包，``implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])``。
(4.add ``implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])`` into **build.gradle** of the moudle you perfer.)
5. 在代码中使用。
(5.use the classes providered with the Har.)

方式二：
使用maven仓集成（Use maven）
```java
allprojects {
    repositories {
        mavenCentral()
    }
}
implementation 'com.gitee.ts_ohos:rxohosaudio:1.0.0'
implementation 'com.gitee.ts_ohos:audioProcessor:1.0.0'
```

## 代码使用(Usage)

#### 录制音频文件(Record to file)

<img src="./pic/audioFileCapture.jpg" alt="audio file capture" style="zoom:25%;" div align=center />

```java
mAudioRecorder = AudioRecorder.getInstance();
mAudioFile = new File(
        Environment.getExternalStorageDirectory().getAbsolutePath() +
                File.separator + System.nanoTime() + ".file.m4a");
Source source = new Source();
source.setRecorderAudioSource(Recorder.AudioSource.MIC);
AudioProperty audioProperty = new AudioProperty.Builder()
                            .setRecorderSamplingRate(AUDIO_SAMPLE_RATE_HZ)
                            .setRecorderBitRate(AUDIO_BIT_RATE_HZ)
                            .setRecorderAudioEncoder(Recorder.AudioEncoder.AAC)
                            .build();
StorageProperty storageProperty = new StorageProperty.Builder()
                            .setRecorderFile(mAudioFile)
                            .setRecorderMaxDurationMs(-1)
                            .setRecorderMaxFileSizeBytes(-1)
                            .build();

mAudioRecorder.prepareRecord(source, Recorder.OutputFormat.MPEG_4,
                            audioProperty, storageProperty);
mAudioRecorder.startRecord();
// ...
mAudioRecorder.stopRecord();
```

#### 播放音频文件(Play a file)

Playconfig 中可以设置音频源，设置音量，或者播放循环：
With PlayConfig, to set audio file or audio resource, set volume, or looping:

<img src="./pic/audioFilePlay.jpg" alt="audio file play" style="zoom:25%;" div align=center />

```java
mRxAudioPlayer.play(PlayConfig.file(audioFile).looping(true).build())
        .subscribeOn(Schedulers.io())
		.observeOn(HarmonySchedulers.mainThread())
        .subscribe(new Observer<Boolean>() {
               @Override
               public void onSubscribe(final Disposable disposable) {

               }

               @Override
               public void onNext(final Boolean aBoolean) {
                    // prepared
               }

               @Override
               public void onError(final Throwable throwable) {

               }

               @Override
               public void onComplete() {
                    // play finished
                    // NOTE: if looping, the Observable will never finish, you need stop playing
                    // onDestroy, otherwise, memory leak will happen!
               }
           });
```

#### PlayConfig 的完整示例(Full example of PlayConfig)

```java
PlayConfig.file(audioFile) // play a local file
    //.res(getApplicationContext(), R.raw.audio_record_end) // or play a raw resource
    .looping(true) // loop or not
    .leftVolume(1.0F) // left volume,but hos Player not support left volume and right volume separated.
    .rightVolume(1.0F) // right volume,but hos Player not support left volume and right volume separated.
    .build(); // build this config and play!
```

#### 录制音频流文件 (Record a stream)

<img src="./pic/audioStreamCapture.jpg" alt="audio stream capture" style="zoom:25%;" div align=center />

```java
mOutputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
        File.separator + System.nanoTime() + ".stream.m4a");
mOutputFile.createNewFile();
mFileOutputStream = new FileOutputStream(mOutputFile);
mStreamAudioRecorder.start(new StreamAudioRecorder.AudioDataCallback() {
    @Override
    public void onAudioData(final byte[] data,final int size) {
        if (mFileOutputStream != null) {
            try {
                mFileOutputStream.write(data, 0, size);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError() {
        new EventHandler(EventRunner.getMainEventRunner()).postSyncTask(new Runnable() {
            @Override
            public void run() {
                new ToastDialog(StreamAbility.this).setText("Record fail").setDuration(DIALOG_DURATION).show();
                mBtnStart.setText("Start");
                mIsRecording = false;
            }
        });
    }
});
```

#### 播放音频流文件 (Play a stream)

<img src="./pic/audioStreamPlay.jpg" alt="audio stream play" style="zoom:25%;" div align=center />

```java
Observable.just(mOutputFile).subscribeOn(Schedulers.io()).subscribe(new Action1<File>() {
    @Override
    public void call(File file) {
        try {
            mStreamAudioPlayer.init();
            FileInputStream inputStream = new FileInputStream(file);
            int read;
            while ((read = inputStream.read(mBuffer)) > 0) {
                mStreamAudioPlayer.play(mBuffer, read);
            }
            inputStream.close();
            mStreamAudioPlayer.release();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
});
```

#### 变音播放 (Change the sound effect in stream mode)

<img src="./pic/audioStreamChangePlay.jpg" alt="audio stream changed play" style="zoom:25%;" div align=center />

``` java
mStreamAudioPlayer.play(
    mAudioProcessor.process(mRatio, mBuffer, StreamAudioRecorder.DEFAULT_SAMPLE_RATE),
    read);
```

## 版本迭代
- v1.0.0 项目初次提交

## 版本和许可信息
- The MIT License (MIT)
- [https://gitee.com/ts_ohos/RxOhosAudio/blob/master/LICENSE](https://gitee.com/ts_ohos/RxOhosAudio/blob/master/LICENSE)

## 欢迎提交修改意见(Contribution are welcome)
https://gitee.com/ts_ohos/RxOhosAudio/pulls
